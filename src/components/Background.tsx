import styled from "@emotion/styled";
import React from "react";
import background from "../images/banner.png";

const Background = () => {
  return <Image src={background} alt="" />;
};

const Image = styled("img")`
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export default Background;
