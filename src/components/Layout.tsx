import { Box, Stack } from "@mui/material";
import React from "react";
import Footer from "./layout/Footer";

interface Props {
  children: React.ReactNode;
}

const Layout: React.FunctionComponent<Props> = ({ children }) => {
  return (
    <Stack minHeight="100vh" justifyItems="center">
      <Box flex={1}>{children}</Box>
      <Footer />
    </Stack>
  );
};

export default Layout;
