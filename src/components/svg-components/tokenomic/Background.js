import * as React from "react";

const SvgBackground = (props) => (
  <svg
    height="100vh"
    preserveAspectRatio="none"
    viewBox="0 0 1440 926"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="m0 0 240 79.036L1227.5 0 1440 54.5V926l-233-89.182L261.5 926 0 868.5V0Z"
      fill="url(#background_svg__a)"
    />
    <defs>
      <linearGradient
        id="background_svg__a"
        x1={-51}
        y1={0}
        x2={1574.39}
        y2={403.407}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#375A64" />
        <stop offset={1} stopColor="#318B8B" />
      </linearGradient>
    </defs>
  </svg>
);

export default SvgBackground;
