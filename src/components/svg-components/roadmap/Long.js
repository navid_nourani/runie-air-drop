import * as React from "react";

const SvgLong = (props) => (
  <svg
    viewBox="-168.459 -32.518 335.912 92.716"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M4.655 2.328A2.328 2.328 0 1 1 2.341 0a2.314 2.314 0 0 1 2.314 2.328ZM4.655 11.04a2.327 2.327 0 1 1-2.314-2.328 2.314 2.314 0 0 1 2.314 2.328ZM4.655 19.672a2.327 2.327 0 1 1-2.314-2.327 2.315 2.315 0 0 1 2.314 2.328Z"
      style={{
        fill: "#fff",
      }}
    />
    <path
      d="M2.359 53.85c6.903 0 12.5-5.597 12.5-12.5 0-6.904-5.597-12.5-12.5-12.5-6.904 0-12.5 5.596-12.5 12.5 0 6.903 5.596 12.5 12.5 12.5Z"
      strokeWidth={6}
      strokeMiterlimit={10}
      strokeLinecap="round"
      style={{
        fill: "#d23b6d",
        paintOrder: "fill",
        stroke: "#fff",
      }}
    />
    <path
      d="m-2.524 4.947 18.079 28.17h-36.158l18.079-28.17Z"
      style={{
        strokeDashoffset: 3,
        vectorEffect: "non-scaling-stroke",
        paintOrder: "fill",
        strokeWidth: 0,
        strokeMiterlimit: 22,
        stroke: "#fff",
        fill: "#fff",
      }}
      transform="rotate(180)"
    />
    <path
      d="M-640.316 41.815c170.66 0 145.406-153.041 318.19-153.041 163.082 0 158.45 153.349 322.347 153.349 166.401 0 157.481-153.349 322.348-153.349 167.465 0 157.607 153.349 322.349 153.349"
      strokeWidth={6}
      strokeMiterlimit={10}
      strokeLinecap="round"
      style={{
        stroke: "#b05252",
      }}
    />
  </svg>
);

export default SvgLong;
