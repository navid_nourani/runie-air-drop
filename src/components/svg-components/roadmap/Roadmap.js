import * as React from "react";

const SvgRoadmap = (props) => (
  <svg
    viewBox="-191.121 0 1385.021 125.401"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M3.125 121.713c131.649 0 112.166-118.057 245.456-118.057 125.806 0 122.233 118.298 248.667 118.298 128.366 0 121.485-118.298 248.667-118.298 129.187 0 121.582 118.298 248.668 118.298M-196.227 121.359h199M994.069 122.257h199"
      strokeWidth={6}
      strokeMiterlimit={10}
      strokeLinecap="round"
      style={{
        stroke: "#fff",
      }}
    />
  </svg>
);

export default SvgRoadmap;
