import * as React from "react";

const SvgRoadmapContinue = (props) => (
  <svg
    width={202}
    height={6}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M3 3h199"
      stroke="#fff"
      strokeWidth={6}
      strokeMiterlimit={10}
      strokeLinecap="round"
    />
  </svg>
);

export default SvgRoadmapContinue;
