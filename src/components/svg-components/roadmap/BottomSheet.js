import * as React from "react";

const SvgBottomSheet = (props) => (
  <svg
    viewBox="-77.561 -32.518 155.285 92.653"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M4.655 2.328A2.328 2.328 0 1 1 2.341 0a2.314 2.314 0 0 1 2.314 2.328ZM4.655 11.04a2.327 2.327 0 1 1-2.314-2.328 2.314 2.314 0 0 1 2.314 2.328ZM4.655 19.672a2.327 2.327 0 1 1-2.314-2.327 2.315 2.315 0 0 1 2.314 2.328Z"
      style={{
        fill: "#fff",
      }}
    />
    <path
      d="M2.359 53.85c6.903 0 12.5-5.597 12.5-12.5 0-6.904-5.597-12.5-12.5-12.5-6.904 0-12.5 5.596-12.5 12.5 0 6.903 5.596 12.5 12.5 12.5Z"
      strokeWidth={6}
      strokeMiterlimit={10}
      strokeLinecap="round"
      style={{
        fill: "#d23b6d",
        paintOrder: "fill",
        stroke: "#fff",
      }}
    />
    <path
      d="m-2.524 4.947 18.079 28.17h-36.158l18.079-28.17Z"
      style={{
        strokeDashoffset: 3,
        vectorEffect: "non-scaling-stroke",
        paintOrder: "fill",
        strokeWidth: 0,
        strokeMiterlimit: 22,
        stroke: "#fff",
        fill: "#fff",
      }}
      transform="rotate(180)"
    />
    <path
      d="M-238.966 43.921c64.25 0 54.742-57.617 119.793-57.617 61.398 0 59.655 57.734 121.36 57.734 62.647 0 59.289-57.734 121.359-57.734 63.049 0 59.337 57.734 121.36 57.734"
      strokeWidth={6}
      strokeMiterlimit={10}
      strokeLinecap="round"
      style={{
        stroke: "#b05252",
      }}
    />
  </svg>
);

export default SvgBottomSheet;
