import * as React from "react";

const SvgMiddlePhoneShadow = (props) => (
  <svg
    width={173}
    height={40}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g opacity={0.16} filter="url(#MiddlePhoneShadow_svg__a)">
      <ellipse
        cx={86.5}
        cy={20}
        rx={80.5}
        ry={14}
        fill="url(#MiddlePhoneShadow_svg__b)"
      />
    </g>
    <defs>
      <radialGradient
        id="MiddlePhoneShadow_svg__b"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="matrix(0 23.5 -135.125 0 86.5 20)"
      >
        <stop />
        <stop offset={1} stopColor="#375A64" stopOpacity={0} />
      </radialGradient>
      <filter
        id="MiddlePhoneShadow_svg__a"
        x={0}
        y={0}
        width={173}
        height={40}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
        <feGaussianBlur
          stdDeviation={3}
          result="effect1_foregroundBlur_101_128"
        />
      </filter>
    </defs>
  </svg>
);

export default SvgMiddlePhoneShadow;
