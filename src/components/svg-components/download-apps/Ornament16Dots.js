import * as React from "react";

const SvgOrnament16Dots = (props) => (
  <svg
    width={130}
    height={30}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M5 27.5a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0Zm25 0a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0ZM52.5 25a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5ZM80 27.5a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0Zm22.5-2.5a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5Zm27.5 2.5a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0ZM2.5 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5ZM30 2.5a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0ZM52.5 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5ZM80 2.5a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0ZM102.5 0a2.5 2.5 0 1 1 0 5 2.5 2.5 0 0 1 0-5ZM130 2.5a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0Z"
      fill="#318B8B"
    />
  </svg>
);

export default SvgOrnament16Dots;
