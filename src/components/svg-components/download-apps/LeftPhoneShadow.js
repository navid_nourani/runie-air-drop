import * as React from "react";

const SvgLeftPhoneShadow = (props) => (
  <svg
    width={171}
    height={52}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g opacity={0.24} filter="url(#LeftPhoneShadow_svg__a)">
      <ellipse
        cx={85.5}
        cy={26}
        rx={80.5}
        ry={14}
        transform="rotate(-9.392 85.5 26)"
        fill="url(#LeftPhoneShadow_svg__b)"
      />
    </g>
    <defs>
      <radialGradient
        id="LeftPhoneShadow_svg__b"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="matrix(.5974 17.8366 -102.56049 3.43508 85.5 26)"
      >
        <stop />
        <stop offset={1} stopColor="#375A64" stopOpacity={0} />
      </radialGradient>
      <filter
        id="LeftPhoneShadow_svg__a"
        x={0.045}
        y={0.938}
        width={170.909}
        height={50.123}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feBlend in="SourceGraphic" in2="BackgroundImageFix" result="shape" />
        <feGaussianBlur
          stdDeviation={3}
          result="effect1_foregroundBlur_101_129"
        />
      </filter>
    </defs>
  </svg>
);

export default SvgLeftPhoneShadow;
