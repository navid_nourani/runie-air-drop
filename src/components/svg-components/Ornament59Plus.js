import * as React from "react";

const SvgOrnament59Plus = (props) => (
  <svg
    width={293}
    height={400}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="m117.706 122.333 118.751-64.426 54.404 100.28-118.75 64.426-1.319.715.715 1.318 64.426 118.751-100.279 54.405-64.426-118.751-.715-1.319-1.319.716-118.75 64.425-54.405-100.279 118.75-64.426 1.319-.715-.715-1.319-64.426-118.75L51.247 2.979l64.425 118.751.716 1.318 1.318-.715Z"
      stroke="#318B8B"
      strokeOpacity={0.42}
      strokeWidth={3}
    />
  </svg>
);

export default SvgOrnament59Plus;
