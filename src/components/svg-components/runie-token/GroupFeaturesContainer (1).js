import * as React from "react";

const SvgGroupFeaturesContainer1 = (props) => (
  <svg
    viewBox="0 0 1261 564"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <defs>
      <radialGradient
        id="GroupFeaturesContainer_(1)_svg__a"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="rotate(46.997 68.105 648.101) scale(331.913)"
      >
        <stop offset={0.04} stopColor="#375A64" />
        <stop offset={0.547} stopColor="#347479" />
        <stop offset={1} stopColor="#318B8B" />
      </radialGradient>
      <radialGradient
        id="GroupFeaturesContainer_(1)_svg__b"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="translate(615.84 282.834) scale(175.23)"
      >
        <stop stopColor="#828282" />
        <stop offset={0.3} stopColor="#848484" stopOpacity={0.98} />
        <stop offset={0.47} stopColor="#8B8B8B" stopOpacity={0.92} />
        <stop offset={0.6} stopColor="#969697" stopOpacity={0.81} />
        <stop offset={0.72} stopColor="#A6A6A7" stopOpacity={0.66} />
        <stop offset={0.83} stopColor="#BBBCBD" stopOpacity={0.47} />
        <stop offset={0.92} stopColor="#D4D5D7" stopOpacity={0.23} />
        <stop offset={1} stopColor="#ECEEF0" stopOpacity={0} />
      </radialGradient>
      <radialGradient
        id="GroupFeaturesContainer_(1)_svg__d"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="translate(814.799 92.166) scale(65.1693)"
      >
        <stop offset={0.04} stopColor="#D8D9DD" />
        <stop offset={0.55} stopColor="#ECEEF0" />
        <stop offset={1} stopColor="#F6F6F7" />
      </radialGradient>
      <radialGradient
        id="GroupFeaturesContainer_(1)_svg__e"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="translate(821.738 498.405) scale(65.1693)"
      >
        <stop offset={0.04} stopColor="#D8D9DD" />
        <stop offset={0.55} stopColor="#ECEEF0" />
        <stop offset={1} stopColor="#F6F6F7" />
      </radialGradient>
      <radialGradient
        id="GroupFeaturesContainer_(1)_svg__f"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="rotate(-180 209.939 46.259) scale(65.4359)"
      >
        <stop offset={0.04} stopColor="#D8D9DD" />
        <stop offset={0.55} stopColor="#ECEEF0" />
        <stop offset={1} stopColor="#F6F6F7" />
      </radialGradient>
      <radialGradient
        id="GroupFeaturesContainer_(1)_svg__g"
        cx={0}
        cy={0}
        r={1}
        gradientUnits="userSpaceOnUse"
        gradientTransform="rotate(180 204.631 249.202) scale(65.1693)"
      >
        <stop offset={0.04} stopColor="#D8D9DD" />
        <stop offset={0.55} stopColor="#ECEEF0" />
        <stop offset={1} stopColor="#F6F6F7" />
      </radialGradient>
      <linearGradient
        id="GroupFeaturesContainer_(1)_svg__c"
        x1={507.857}
        y1={295.288}
        x2={728.435}
        y2={295.288}
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#fff" />
        <stop offset={1} stopColor="#D5D5D5" />
      </linearGradient>
    </defs>
    <path
      d="M762.516 293.125a145.795 145.795 0 0 0-36.723-96.948 146.298 146.298 0 0 0-252.297 93.353 146.296 146.296 0 0 0 36.888 104.618 146.299 146.299 0 0 0 252.132-101.023Z"
      fill="url(#GroupFeaturesContainer_(1)_svg__a)"
    />
    <path
      d="m535.839 370.753 68.178 68.17c4.013.331 8.062.51 12.191.51a146.3 146.3 0 0 0 103.455-42.852 146.314 146.314 0 0 0 42.853-103.456c0-2.317-.057-4.622-.17-6.915l-49.629-49.629-176.878 134.172Z"
      fill="url(#GroupFeaturesContainer_(1)_svg__b)"
      style={{
        mixBlendMode: "multiply",
      }}
    />
    <path
      d="M728.364 293.609a111.72 111.72 0 0 1-190.714 78.996 111.713 111.713 0 0 1 16.93-171.886 111.712 111.712 0 0 1 104.819-10.324 111.714 111.714 0 0 1 68.965 103.214Z"
      fill="#fff"
    />
    <path
      d="M728.435 293.73a111.27 111.27 0 0 0-28.053-74.046 111.714 111.714 0 0 0-141.42-21.597 111.718 111.718 0 1 0 169.473 95.625v.018Z"
      fill="url(#GroupFeaturesContainer_(1)_svg__c)"
    />
    <path
      d="M728.364 293.609a111.268 111.268 0 0 0-28.053-74.045 111.716 111.716 0 0 0-189.258 37.554 111.717 111.717 0 1 0 217.311 36.473v.018Z"
      stroke="#fff"
      strokeWidth={0.152}
      strokeMiterlimit={10}
    />
    <path
      d="M717.069 244.642a111.725 111.725 0 0 1-192.355 111.225 112.878 112.878 0 0 0 11.116 14.886 111.74 111.74 0 0 0 69.616 33.979 111.74 111.74 0 0 0 119.288-83.004 111.733 111.733 0 0 0-7.665-77.086Z"
      fill="#ECEEF0"
    />
    <path
      d="M1223.33 93.632c.02 13.249-3.32 25.219-8.7 33.861-5.39 8.648-12.77 13.898-20.85 13.898H799.686c-8.062 0-15.438-5.249-20.824-13.898-5.381-8.642-8.725-20.612-8.725-33.861 0-13.245 3.34-25.213 8.718-33.853 5.384-8.648 12.759-13.897 20.831-13.897h394.094c8.07 0 15.44 5.249 20.83 13.897 5.38 8.64 8.72 20.608 8.72 33.853Z"
      fill="#fff"
      stroke="#318B8B"
    />
    <path
      d="M860.874 138.248c25.45-25.45 25.45-66.713 0-92.163-25.451-25.45-66.713-25.45-92.164 0-25.45 25.45-25.45 66.713 0 92.163 25.451 25.45 66.713 25.45 92.164 0Z"
      fill="url(#GroupFeaturesContainer_(1)_svg__d)"
    />
    <path
      d="M867.641 93.443a53.981 53.981 0 1 1-107.963.001 53.981 53.981 0 0 1 107.963 0Z"
      fill="#fff"
    />
    <path
      d="M867.618 93.52a53.743 53.743 0 0 0-13.552-35.774 53.981 53.981 0 0 0-79.482 73.042 53.968 53.968 0 0 0 59.124 12.845 53.973 53.973 0 0 0 33.901-50.114h.009Z"
      fill="#318B8B"
    />
    <path
      d="M1230.75 547.623H806.655c-8.059 0-15.432-5.246-20.817-13.893-5.38-8.639-8.724-20.607-8.724-33.856 0-13.245 3.34-25.213 8.718-33.854 5.382-8.647 12.755-13.896 20.823-13.896h424.095c8.06 0 15.44 5.249 20.83 13.896 5.38 8.641 8.72 20.609 8.72 33.854 0 13.249-3.34 25.217-8.72 33.856-5.39 8.647-12.77 13.893-20.83 13.893Z"
      fill="#fff"
      stroke="#318B8B"
    />
    <path
      d="M821.738 563.574c35.992 0 65.169-29.177 65.169-65.169s-29.177-65.17-65.169-65.17-65.169 29.178-65.169 65.17c0 35.992 29.177 65.169 65.169 65.169Z"
      fill="url(#GroupFeaturesContainer_(1)_svg__e)"
    />
    <path
      d="M874.592 499.685a53.987 53.987 0 0 1-33.324 49.874 53.989 53.989 0 0 1-58.83-11.702 53.981 53.981 0 0 1 38.171-92.154 53.985 53.985 0 0 1 53.983 53.982Z"
      fill="#fff"
    />
    <path
      d="M874.947 499.982a53.972 53.972 0 0 0-33.311-49.87 53.978 53.978 0 0 0-58.821 11.69 53.98 53.98 0 0 0-11.71 58.818 53.975 53.975 0 0 0 49.859 33.327 53.833 53.833 0 0 0 20.68-4.064 53.84 53.84 0 0 0 29.232-29.223 53.826 53.826 0 0 0 4.071-20.678Z"
      fill="#318B8B"
    />
    <path
      d="M874.592 499.685a53.975 53.975 0 0 0-92.132-38.18 53.97 53.97 0 0 0-11.71 58.818 53.976 53.976 0 0 0 49.859 33.327 53.833 53.833 0 0 0 38.213-15.757 53.826 53.826 0 0 0 15.77-38.208Z"
      stroke="#fff"
      strokeWidth={0.152}
      strokeMiterlimit={10}
    />
    <path
      d="M869.137 476.013a53.995 53.995 0 0 1-22.339 68.608 53.993 53.993 0 0 1-70.605-14.868 53.809 53.809 0 0 0 71.529 16.467 53.81 53.81 0 0 0 21.415-70.207Z"
      fill="#D8D9DD"
    />
    <path
      d="M431.287 141.391H37.19c-8.063 0-15.439-5.249-20.824-13.898-5.381-8.642-8.726-20.612-8.726-33.861 0-13.245 3.345-25.213 8.726-33.853C21.75 51.13 29.127 45.882 37.19 45.882h394.097c8.072 0 15.447 5.25 20.831 13.897 5.378 8.64 8.719 20.608 8.719 33.853 0 13.249-3.345 25.219-8.726 33.861-5.386 8.649-12.761 13.898-20.824 13.898Z"
      fill="#fff"
      stroke="#318B8B"
    />
    <path
      d="M462.284 138.245c25.45-25.45 25.45-66.713 0-92.163-25.45-25.45-66.713-25.45-92.164 0-25.45 25.45-25.45 66.713 0 92.163 25.451 25.45 66.713 25.45 92.164 0Z"
      fill="url(#GroupFeaturesContainer_(1)_svg__f)"
    />
    <path
      d="M363.359 93.443a53.981 53.981 0 1 0 107.963.001 53.981 53.981 0 0 0-107.963 0Z"
      fill="#fff"
    />
    <path
      d="M363 93.395a53.74 53.74 0 0 1 13.57-35.773 53.98 53.98 0 0 1 75.494-2.385 53.978 53.978 0 0 1 3.979 75.427 53.976 53.976 0 0 1-59.139 12.865A53.975 53.975 0 0 1 363 93.395Z"
      fill="#318B8B"
    />
    <path
      d="M363.359 93.444a53.74 53.74 0 0 1 13.569-35.774 53.982 53.982 0 0 1 79.473 73.043 53.971 53.971 0 0 1-83.778-7.003 53.97 53.97 0 0 1-9.264-30.266Z"
      stroke="#fff"
      strokeWidth={0.152}
      strokeMiterlimit={10}
    />
    <path
      d="M368.813 69.771a53.984 53.984 0 0 0 60.481 74.729 53.986 53.986 0 0 0 32.455-20.989 53.517 53.517 0 0 1-5.374 7.166 53.996 53.996 0 0 1-69.818 6.976 53.993 53.993 0 0 1-17.753-67.882h.009Z"
      fill="#D8D9DD"
    />
    <path
      d="M401.696 93.032c0-3.69.661-6.581 1.984-8.672 1.344-2.09 3.563-3.136 6.656-3.136 3.093 0 5.301 1.045 6.624 3.136 1.344 2.09 2.016 4.981 2.016 8.672 0 3.712-.672 6.624-2.016 8.736-1.323 2.112-3.531 3.168-6.624 3.168-3.093 0-5.312-1.056-6.656-3.168-1.323-2.112-1.984-5.024-1.984-8.736Zm12.864 0c0-1.579-.107-2.901-.32-3.968-.192-1.088-.597-1.973-1.216-2.656-.597-.683-1.493-1.024-2.688-1.024s-2.101.341-2.72 1.024c-.597.683-1.003 1.568-1.216 2.656-.192 1.067-.288 2.39-.288 3.968 0 1.621.096 2.987.288 4.096.192 1.088.597 1.973 1.216 2.656.619.661 1.525.992 2.72.992 1.195 0 2.101-.331 2.72-.992.619-.683 1.024-1.568 1.216-2.656.192-1.11.288-2.475.288-4.096Zm7.28-7.232v-4.128h7.712V105h-4.608V85.8h-3.104ZM795.696 93.032c0-3.69.661-6.581 1.984-8.672 1.344-2.09 3.563-3.136 6.656-3.136 3.093 0 5.301 1.045 6.624 3.136 1.344 2.09 2.016 4.981 2.016 8.672 0 3.712-.672 6.624-2.016 8.736-1.323 2.112-3.531 3.168-6.624 3.168-3.093 0-5.312-1.056-6.656-3.168-1.323-2.112-1.984-5.024-1.984-8.736Zm12.864 0c0-1.579-.107-2.901-.32-3.968-.192-1.088-.597-1.973-1.216-2.656-.597-.683-1.493-1.024-2.688-1.024s-2.101.341-2.72 1.024c-.597.683-1.003 1.568-1.216 2.656-.192 1.067-.288 2.39-.288 3.968 0 1.621.096 2.987.288 4.096.192 1.088.597 1.973 1.216 2.656.619.661 1.525.992 2.72.992 1.195 0 2.101-.331 2.72-.992.619-.683 1.024-1.568 1.216-2.656.192-1.11.288-2.475.288-4.096Zm9.232 6.912c2.048-1.707 3.68-3.125 4.896-4.256 1.216-1.152 2.229-2.347 3.04-3.584.81-1.237 1.216-2.453 1.216-3.648 0-1.088-.256-1.941-.768-2.56s-1.302-.928-2.368-.928c-1.067 0-1.888.363-2.464 1.088-.576.704-.875 1.675-.896 2.912h-4.352c.085-2.56.842-4.501 2.272-5.824 1.45-1.323 3.285-1.984 5.504-1.984 2.432 0 4.298.65 5.6 1.952 1.301 1.28 1.952 2.976 1.952 5.088 0 1.664-.448 3.253-1.344 4.768-.896 1.515-1.92 2.837-3.072 3.968-1.152 1.11-2.656 2.453-4.512 4.032h9.44v3.712h-15.808v-3.328l1.664-1.408Z"
      fill="#fff"
    />
    <path
      d="M424.336 547.623H30.24c-8.059 0-15.432-5.246-20.817-13.893C4.043 525.091.7 513.123.7 499.874c0-13.245 3.344-25.213 8.724-33.854 5.385-8.647 12.758-13.896 20.817-13.896h394.096c8.068 0 15.441 5.249 20.823 13.896 5.378 8.641 8.718 20.609 8.718 33.854 0 13.249-3.344 25.217-8.724 33.856-5.385 8.647-12.758 13.893-20.817 13.893Z"
      fill="#fff"
      stroke="#318B8B"
    />
    <path
      d="M409.253 563.574c35.992 0 65.169-29.177 65.169-65.169s-29.177-65.17-65.169-65.17-65.169 29.178-65.169 65.17c0 35.992 29.177 65.169 65.169 65.169Z"
      fill="url(#GroupFeaturesContainer_(1)_svg__g)"
    />
    <path
      d="M356.408 499.685a53.982 53.982 0 1 0 83.974-44.884 53.983 53.983 0 0 0-83.974 44.884Z"
      fill="#fff"
    />
    <path
      d="M356 499.982a53.971 53.971 0 0 1 64.494-52.947 53.97 53.97 0 0 1 42.414 42.4 53.98 53.98 0 0 1-3.066 31.185 53.978 53.978 0 0 1-49.86 33.327 53.832 53.832 0 0 1-38.212-15.758A53.824 53.824 0 0 1 356 499.982Z"
      fill="#318B8B"
    />
    <path
      d="M356.408 499.685a53.975 53.975 0 0 1 92.132-38.18 53.97 53.97 0 0 1 11.71 58.818 53.976 53.976 0 0 1-49.859 33.327 53.824 53.824 0 0 1-53.983-53.965Z"
      stroke="#fff"
      strokeWidth={0.152}
      strokeMiterlimit={10}
    />
    <path
      d="M361.863 476.013a53.995 53.995 0 0 0 22.339 68.608 53.993 53.993 0 0 0 70.605-14.868 53.809 53.809 0 0 1-71.529 16.467 53.81 53.81 0 0 1-21.415-70.207Z"
      fill="#D8D9DD"
    />
    <path
      d="M391.696 499.032c0-3.691.661-6.581 1.984-8.672 1.344-2.091 3.563-3.136 6.656-3.136 3.093 0 5.301 1.045 6.624 3.136 1.344 2.091 2.016 4.981 2.016 8.672 0 3.712-.672 6.624-2.016 8.736-1.323 2.112-3.531 3.168-6.624 3.168-3.093 0-5.312-1.056-6.656-3.168-1.323-2.112-1.984-5.024-1.984-8.736Zm12.864 0c0-1.579-.107-2.901-.32-3.968-.192-1.088-.597-1.973-1.216-2.656-.597-.683-1.493-1.024-2.688-1.024s-2.101.341-2.72 1.024c-.597.683-1.003 1.568-1.216 2.656-.192 1.067-.288 2.389-.288 3.968 0 1.621.096 2.987.288 4.096.192 1.088.597 1.973 1.216 2.656.619.661 1.525.992 2.72.992 1.195 0 2.101-.331 2.72-.992.619-.683 1.024-1.568 1.216-2.656.192-1.109.288-2.475.288-4.096Zm7.888-5.216c.106-2.133.853-3.776 2.24-4.928 1.408-1.173 3.253-1.76 5.536-1.76 1.557 0 2.89.277 4 .832 1.109.533 1.941 1.269 2.496 2.208.576.917.863 1.963.863 3.136 0 1.344-.351 2.485-1.055 3.424-.683.917-1.504 1.536-2.464 1.856v.128c1.237.384 2.197 1.067 2.88 2.048.704.981 1.056 2.24 1.056 3.776 0 1.28-.299 2.421-.896 3.424-.576 1.003-1.44 1.792-2.592 2.368-1.131.555-2.496.832-4.096.832-2.411 0-4.374-.608-5.888-1.824-1.515-1.216-2.315-3.008-2.4-5.376h4.352c.042 1.045.394 1.888 1.056 2.528.682.619 1.61.928 2.784.928 1.088 0 1.92-.299 2.496-.896.597-.619.896-1.408.896-2.368 0-1.28-.406-2.197-1.216-2.752-.811-.555-2.07-.832-3.776-.832h-.928v-3.68h.928c3.029 0 4.544-1.013 4.544-3.04 0-.917-.278-1.632-.832-2.144-.534-.512-1.312-.768-2.337-.768-1.002 0-1.781.277-2.335.832-.534.533-.843 1.216-.928 2.048h-4.384ZM801.696 499.032c0-3.691.661-6.581 1.984-8.672 1.344-2.091 3.563-3.136 6.656-3.136 3.093 0 5.301 1.045 6.624 3.136 1.344 2.091 2.016 4.981 2.016 8.672 0 3.712-.672 6.624-2.016 8.736-1.323 2.112-3.531 3.168-6.624 3.168-3.093 0-5.312-1.056-6.656-3.168-1.323-2.112-1.984-5.024-1.984-8.736Zm12.864 0c0-1.579-.107-2.901-.32-3.968-.192-1.088-.597-1.973-1.216-2.656-.597-.683-1.493-1.024-2.688-1.024s-2.101.341-2.72 1.024c-.597.683-1.003 1.568-1.216 2.656-.192 1.067-.288 2.389-.288 3.968 0 1.621.096 2.987.288 4.096.192 1.088.597 1.973 1.216 2.656.619.661 1.525.992 2.72.992 1.195 0 2.101-.331 2.72-.992.619-.683 1.024-1.568 1.216-2.656.192-1.109.288-2.475.288-4.096Zm7.632 7.424v-3.52l10.464-14.976h5.216v14.592h2.816v3.904h-2.816V511h-4.48v-4.544h-11.2Zm11.488-13.696-6.56 9.792h6.56v-9.792Z"
      fill="#fff"
    />
  </svg>
);

export default SvgGroupFeaturesContainer1;
