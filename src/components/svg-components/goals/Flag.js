import * as React from "react";

const SvgFlag = (props) => (
  <svg
    width={40}
    height={40}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    {...props}
  >
    <mask
      id="Flag_svg__b"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={40}
      height={40}
    >
      <path fill="url(#Flag_svg__a)" d="M0 0h40v40H0z" />
    </mask>
    <g mask="url(#Flag_svg__b)">
      <path fill="#fff" d="M0 0h39.688v40H0z" />
    </g>
    <defs>
      <pattern
        id="Flag_svg__a"
        patternContentUnits="objectBoundingBox"
        width={1}
        height={1}
      >
        <use xlinkHref="#Flag_svg__c" transform="scale(.00195)" />
      </pattern>
      <image
        id="Flag_svg__c"
        width={512}
        height={512}
        xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOxAAADsQBlSsOGwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABeZSURBVHic7d1rrGVnWQfw/3QunbbTTqe1VyidtraVFgq1xBiutkXAS71hJYQUAjF+0YREIgbRpB/kYiJRYvxAYmJUMCokoASCUJWoISpULrYF0qFpoXaA0nZ6oVc69cNax3N6embOZe+137Xe9/dL3uzdc+bynHU6+/2ftZ5n7QS27kCSpwqsKxfxxQHU7JjSBQAAiycAAECDBAAAaJAAAAANEgAAoEECAAA0SAAAgAYJAADQIAEAABokAABAgwQAAGiQAAAADRIAAKBBAgAANGhH6QKAhTkmyd4kJyXZ068Tk5zcPz+u/1yS7OsfT0z3OnF8kmOT7EpyQpLtK37tkqXPrbb0Z6x0f5LDR6n1ySQP9M8fS/Jw//z7SR5f9Wcc7p8/kuTR/vljSR7q12P9xx7tf82h/s/+wVH+fqieAADTsj3JqUlOWfF4yqqPLa2T+3Viljf4sdi3/i8Z3MPpgsDSOpQuKNy/4mP3Jbknyb39Wvn8aAEGRk8AgHE4vV9nJzmzX2clOSPJs/rPnZluQ2c+ju/XmVv8/SvDwfeSfDfJwSTf6dddKz52/6zFwrwJADC8M5M8Z8U6t19n9+v0JDuLVcdW7cvGz2Q8mmeGgrWCwnfSXaaAwQkAMJttSZ6d5IezvLGfm6dv+McWq46x2J3l/zfW80C6UPCtJN/sH2/vny/99+NH+s2wUQIAbMy+JOf369Ikl/TPL053fR3m5aR+/chRfs19SW5Ld9bgrv75bSs+djDJU8OWydQJALBsV5LnZnmDv3DFsskzJvuSXHGUzz+U5I4V6xtJbk1yoH/+2NAFMn4CAC3anu5U7NJGv/T4vDhdTx32pPv/+tIjfH7pDMItSW7O8tmDr6cLDzRAAKB2ZyT50SSXpdvglzZ7Gz0tWzqDsPoswlNJ7kx3pmD1+nqcOaiKAEBNTk63yV+xYl1StCKYlm1JzunXlWt8/mC6Mwa3rHj8SpZv2sSECABMlc0eFu+sfr1y1cfvy9NDwc39OrjQ6tiUbaULYNIOJLmgwN97KG6IA1Nwd7og8NUkX07yP/16sGRRdAQAZlEqAADTdjDJjVk+Y3Bjkq+lew8IFkQAYBYCADAvD6cLBCvPFHwp3eUFBiAAMAsBABja0tmCpfX5JN8uWlElBABmIQAAJawMBSsbD9kEAYBZCADAWBxKFwJWni24JW6JfEQCALMQAIAxuzfJf69aByIUJBEAmI0AAEzNg+luXrTyTEGTEwgCALMQAIAaPJRu4uArSW5Kd+ngpiT3lCxqaAIAsxAAgFodTnJ1ks8WrmMwx5QuAABG6HdT8eafCAAAsNrHkry3dBFDEwAAYNnXkrwpDUwKCAAA0LkvyTVp5O2NBQAA6Jr+rkvX3NwEAQAAuqa/T5QuYpEEAABa10TT32oCAAAta6bpbzUBAIBWNdX0t5oAAECLmmv6W00AAKBFzTX9rSYAANCaJpv+VhMAAGhJs01/qwkAALSi6aa/1QQAAFrQfNPfagIAAC1ovulvNQEAgNpp+luDAABAzTT9HYEAAECtNP0dhQAAQI00/a1DAACgRpr+1iEAAFAbTX8bIAAAUBNNfxskAABQC01/myAAAFADTX+bJAAAUANNf5skAAAwdZr+tkAAAGDKNP1tkQAAwFRp+puBAADAFGn6m5EAAMAUafqbkQAAwNRo+psDAQCAKdH0NycCAABToelvjgQAAKZA09+cCQAATIGmvzkTAAAYO01/AxAAABgzTX8DEQAAGCtNfwMSAAAYI01/AxMAABgjTX8DEwAAGBtNfwsgAAAwJpr+FkQAAGAsNP0tkAAAwBho+lswAQCAMdD0t2ACAAClaforQAAAoCRNf4UIAACUoumvIAEAgBI0/RUmAABQgqa/wgQAABZN098ICAAALJKmv5EQAABYFE1/IyIAALAImv5GRgAAYBF+L5r+RkUAAGBoH0vyntJF8HQCAABD0vQ3UgIAAEPR9DdiAgAAQ9D0N3ICAABD0PQ3cgIAAPOm6W8CBAAA5knT30QIAADMi6a/CREAAJgHTX8TIwAAMA+a/iZGAABgVpr+JkgAAGAWmv4mSgAAYKs0/U2YAADAVmj6mzgBAICt0PQ3cQIAAJul6a8CAgAAm6HprxICAAAbpemvIgIAABuh6a8yAgAAG6HprzICAADr0fRXIQEAgKPR9FcpAQCAI9H0VzEBAIC1aPqrnAAAwFo0/VVOAABgNU1/DRAAAFhJ018jBAAAlmj6a4gAAECi6a85AgAASXJ9NP01RQAAIEl2ly6AxRIAAEiStyW5oHQRLI4AAECSHJvk3aWLYHEEAACW/EqSl5cugsUQAABY6Y9jb2iCbzIAK12e7kZAVE4AAGC19yQ5qXQRDEsAAGC1M5L8dukiGJYAAMBajAVWTgAAYC3GAisnAABwJMYCKyYAAHA0xgIr5ZsKwNEYC6yUAADAeowFVkgAAGA9xgIrJAAAsBHGAisjAACwEcYCKyMAALBRxgIrIgAAsBnGAivhmwjAZhgLrIQAAMBmGQusgAAAwGYZC6yAAADAVhgLnDgBAICtMBY4cQIAAFtlLHDCBAAAZmEscKJ80wCYhbHAiRIAAJiVscAJEgAAmJWxwAkSAACYB2OBEyMAADAPxgInRgAAYF6MBU6IAADAPBkLnAjfJADmyVjgRAgAAMybscAJEAAAmDdjgRMgAAAwBGOBIycAADAEY4EjJwAAMBRjgSMmAAAwJGOBI+WbAsCQjAWOlAAAwNCMBY6QAADA0IwFjpAAAMAiGAscGQEAgEUwFjgyAgAAi2IscEQEAAAWyVjgSPgmALBIxgJHQgAAYNGMBY6AAADAohkLHAEBAIASjAUWJgAAUIKxwMIEAABKMRZYkAAAQEnGAgtx0AEoyVhgIQIAAKUZCyxAAACgNGOBBQgAAIyBscAFEwAAGANjgQsmAAAwFsYCF0gAAGBMjAUuiIMMwJgYC1wQAQCAsTEWuAACAABjYyxwAQQAAMbIWODABAAAxshY4MAEAADGyljggAQAAMbMWOBAHFQAxsxY4EAEAADGzljgAAQAAMbOWOAABAAApsBY4JwJAABMgbHAORMAAJgKY4FzJAAAMCXGAufEQQRgSowFzokAAMDUGAucAwEAgKkxFjgHAgAAU2QscEYCAABTZCxwRgIAAFNlLHAGAgAAU2YscIscNACmzFjgFgkAAEydscAtEAAAmDpjgVsgAABQA2OBmyQAAFADY4GbJAAAUAtjgZsgAABQE2OBG+QgAVATY4EbJAAAUBtjgRsgAABQG2OBGyAAAFAjY4HrEAAAqJGxwHUIAADUyljgUQgAANTMWOAROCgA1MxY4BEIAADUzljgGgQAAGpnLHANAgAALTAWuIoAAEALjAWuIgAA0ApjgSsIAAC0xFhgz0EAxuLBJPclebx0IVTNWGBvW+kCmLQD0VRTk0fTbcJLG/HS8wf6x/uTPNL/uqXHR9f42CNJHkvycP/nLn18pe9n9o1+T5Kd/fO96X6g2ZHkxP5jJyTZle517uT+83v7j+9e5/mJ/e/Zm+7aMXU5mOSiJA+VLqSkHaULAObunqOs7/XrnnQb+gNJDqXb4J8oUewMVr543zfg33NclsPAyauen5Lkh/p1av94epLT0gUUxumsJO9I8s7ShZTkDACzcAZgce5O8p0kdyX5drqfYA72z+9K8t0sb/JPFaqRp9ud5XBwerpZ9DOTnN0/f1b/8Wdl+awFi/Nokucmub1wHcUIAMxCAJjdE0nuTPLNJHf062CS/023qd/ZP7ouXrfj0wWDM5Ock+TZ/Tp3xfMzilVXrw+nmwxokgDALASA9T2QbnO/vX9cuW5Pt9kfLlQb07I7y2Fgf5Lz0/37W3o8rVhl0/VUklck+bfShZQgADALAaBzd7pjcWu/DvTrGxn22jSsdFKWw8AFq56fEz1fR3Jjkh9Lg0FcAGAWLQWA72Z5kz+w6vn9BeuCjdiZ7nLC6mBwUZILszxN0aq3JPnz0kUsmgDALGoMAN9K8tUkN/WPN/ePh0oWBQPamS4EXNKvS9M1x12cboyyBU2OBQoAzGLKAeBgus39tiS39M+/nO50PtBdMnhOukBwSbozB5cmeWG6eybU5t1pbCxQAGAWUwgAD6Tb2L/Ur6Wf7B8sWRRM2PYk5yV5XpYDweXpAsKU95TmxgKn/M2ivLEFgPvS/TR/44r11TTY3AMFnJjkBVm+jHBFv3aXLGqT/i7J60oXsSgCALMoGQAO5ukb/Rf6jwHjsSNdL8FSGLgk3dmCU0sWtY5XJPnX0kXA2B1IN0e76PWqRXxxwCCOSddw94Z078z3uXTvF1HitWSt9YV4ozxYV6kAcOUivjhgYXaku2zwxiTvT7cJP5lyIeDNw365MH0CADCUk5Jcne5Nez6a7j0vFvUac1e8mRMclQAALNLZSa7NYs4SvGtBXxNMkgAAlHRiklcmuT7Jx9PdsGterzOPZlxTTjAqAgAwJku9BL+W5C/TzfTP8lrztwutHiZEAADGbtbLBi9ffMkwfgIAMDX7kvxMkt9P8i/p7v9/tNcbY4GwBgEAmLrtefplg5vzzNecKscC3QmQWZS6E+BV6ZI7wBDOSndb4xeke6+D05Jck+6GRdXYUboAABiZg/36VOlChuS6BgA0SAAAgAYJAADQIAEAABokAABAgwQAAGiQAAAADRIAAKBBAgAANEgAAIAGCQAA0CABAAAaJAAAQIMEAABokAAAAA0SAACgQQIAADRIAACABgkAANAgAQAAGiQAAECDBAAAaJAAAAANEgAAoEECAAA0SAAAgAYJAADQIAEAABokAABAgwQAAGiQAAAADRIAAKBBAgAANEgAAIAGCQAA0CABAAAaJAAAQIMEAABokAAAAA0SAACgQQIAADRIAACABgkAANAgAQAAGiQAAECDBAAAaJAAAAANEgAAoEECAAA0SAAAgAYJAADQIAEAABokAABAgwQAAGiQAAAADRIAAKBBAgAANEgAAIAGCQAA0CABAAAaJAAAQIMEAABokAAAAA0SAACgQQIAADRIAACABgkAANAgAQAAGiQAAECDBAAAaJAAAAANEgAAoEECAAA0SAAAgAYJAADQIAEAABokAABAgwQAAGiQAAAADRIAAKBBAgAANEgAAIAGCQAA0CABAAAaJAAAQIMEAABokAAAAA0SAACgQQIAADRIAACABgkAANAgAQAAGiQAAECDBAAAaJAAAAANEgAAoEECAAA0SAAAgAYJAADQIAEAABokAABAgwQAAGiQAAAADRIAAKBBAgAANEgAAIAGCQAA0CABAAAaJAAAQIMEAABokAAAAA0SAACgQQIAADRIAACABgkAAACs69gk1yb5TJLDSZ4qsO5K8t4k5w/8tQJA805Lt+nemzKb/lrrySSfSPLiAb9uAGjS6Unel+ShlN/wj7ZuSPKygY4BADRjR5K3JjmU8pv7ZtbHk+yf/+EAgPq9LMnNKb+Zb3V9P8nvpAsxAMA6dia5PskPUn4Tn8f6zyQXzfMAAUBtLkzyxZTftOe9Hkry5jkeJwCoxqszru7+IdYH0p3hAACSvC31nPJfb/1zkn3zOWwAMF3Xp/ymvOh1c5Kz53DsAGBytiX5o5TfjEutryU5Z+ajCAAT876U34RLrwNJzpj1QALAVLwj5TffsawvJ9k72+EEgPG7LuXewGes69NxwyCgQdtLF8DCXJbkY0l2lS5kZC5IsjvdewkAQFX2pbvmXfqn7bGuw0l+ectHFwBG6iMpv8mOfR1K8pytHmAAGJvXp/zmOpV1Q7oRSQCYtNOSfCflN9YprV/d0pEGgBH5y5TfUKe27k1y6lYONgCMweVJnkz5DXWK6/1bON4AMAo3pPxGOtX1eJKLNn/IAaCsn0z5TXTq60ObPuoAUNg/pfwGOvX1g3Q3CQKo0jGlC2DuXpTkqtJFVGB7kreWLgIANkrn//zWQ0lO2tzhB5gGZwDqckKSXyxdREVOSPLa0kUADEEAqMu1SfaULqIybypdAMAQBIC6vKF0ARV6eZJzSxcBMG8CQD1OTLdZMV/bkvx06SIA5k0AqMcrk+wqXUSlXlO6AIB5EwDq8VOlC6jY1UmOLV0EwDwJAPV4aekCKnZCkheWLgJgngSAOuxJcnHpIir3otIFAMyTAFCHy+N7ObQrShcAME82jTpcVrqABrygdAEA8yQA1OG80gU0YH/pAgDmSQCogxvVDO+UdPdaAKiCAFCH55QuoBGOM1ANAaAOp5YuoBGOM1ANAaAOx5cuoBEnlC4AYF4EgDoIAIvhOAPV2FG6AObig0l2li6iAXeWLgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARmtb6QLYlOOSnNqvPf1/s3iHk9yf5IEkdyc5VLYcgM0TAMZnZ5JLkzy/Xxcl2Z/kvCQnlSuLo3giyTeT3J7ktiQ39etLSe4tVxbAkQkA5W1P8sIkL03ykiSvSrK3aEXM021Jblix7itbDkBHAChjT5JfSvLaJK+IDb8VTya5Mcmnknwwya1lywFgEY5J91P+B5I8mOQpq/n1hSRvTXJaAKjO6UneleTOlN9wrHGuR5N8JN0lIAAm7swk703y/ZTfYKzprH9Pck0AmJzzkrw/ySMpv5lY011fTPLGdJeOABix45Ncn+SxlN88rHrW55O8KACM0jVJ7kj5zcKqcz2R7qyS+0AAjMQFST6Z8huE1ca6K91lAQAKemOSh1J+U7DaWx9Nsi8ALNSedDdxKb0JWG2v25P8eAC2aHvpAibmkiT/mOTq0oXQvJOTvCnJ40k+V7gWgKpdl+ThlP/Jz7JWrw/HO0MCDOKt6e7jXvqF3rKOtP4j3dtEAzAH29Ldza/0i7tlbWTdnOTZAWAm25P8Wcq/qFvWZtbtSS4OwDq8HfDadqR7c5afL10IbMHdSa5Md0YAYE3uM/5M29K9Za/Nn6k6Lcmnk5xbuhBgvASAZ/qDJG8pXQTM6Owkn0n3dtQAz+ASwNP9RpI/KV0EzNHnk1yV7q6VAP9PAFj2hiR/FceE+nwyyc+lG2UFSOJOgEsuTfIPSXaVLgQGcGG6CYHPFq4DGBE/7SYnJPmvdLf5hVodTvKadH0BAJoAk/xpbP7U75h0l7jOKl0IMA6tB4DXp3tDFWjBGUn+Oi79AWn7hWB/kk/FdX/asj/Jg/EOgtC8lnsA/j5dZzS05uF0l73uKF0IUE6rlwBeE5s/7To+yR+WLgIoq8VLAMcl+USSfaULgYIuSXeToFtLFwKU0eIZgHcmOb90ETAC70+yu3QRQBmtnQE4J8nfpHu3P2jdKUkeiIZAaFJrZwB+K8mxpYuAEXlbustiQGNaOgNwepK/SLKzdCEwInuSfDvd3TCBhrR0BuA303U/A0/39rgfBjSnlTMAe5N8KBqeYC17090T4IulCwEWp5UzAL+e7kUOWNvb0/aNwaA5rQSAN5YuAEbuoiQvKV0EsDgtBIAXJ7m4dBEwAdeVLgBYnBYCgBc12JjXxUggNKP2ALArybWli4CJ2JvkZ0sXASxG7QHgmiSnli4CJsQZM2hE7QHgF0oXABPz6iQnlC4CGF7tAeDK0gXAxOxK1zgLVK7mAHBxkmeVLgImSHCGBtQcAK4qXQBMlH870ICaA4CfYmBrXpTk5NJFAMOqNQBsS/ITpYuAidqe5GWliwCGVWsA2J/ktNJFwIRdUboAYFi1BoDLShcAE+ffEFSu1gDwvNIFwMQ9v3QBwLBqDQAXli4AJm5/kh2liwCGU2sA2F+6AJi4HUmeXboIYDgCAHAk55UuABhOrQHgjNIFQAXOLF0AMJwaA8CeJLtLFwEV8E6aULEaA8AppQuASggAULEaA8Ce0gVAJY4vXQAwnBoDwK7SBUAlji1dADCcGgPAztIFQCUEAKhYjQFge+kCoBL+LUHFagwAAMA6BAAAaJAAAAANEgAAoEECAAA0SAAAgAYJAADQIAEAABokAABAgwQAAGiQAAAADRIAAKBBAgAANEgAAIAG/R8ncllnBtYMswAAAABJRU5ErkJggg=="
      />
    </defs>
  </svg>
);

export default SvgFlag;
