import * as React from "react";

const SvgGroup12LeftTop = (props) => (
  <svg
    height={47}
    viewBox="0 0 456 47"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M449 41.5 408.5 1H0" stroke="#fff" />
    <circle r={5} transform="matrix(-1 0 0 1 450.5 41.5)" fill="#fff" />
  </svg>
);

export default SvgGroup12LeftTop;
