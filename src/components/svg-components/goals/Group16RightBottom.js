import * as React from "react";

const SvgGroup16RightBottom = (props) => (
  <svg
    height={47}
    viewBox="0 0 436 47"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M6.5 5.5 47 46h388.5" stroke="#fff" />
    <circle r={5} transform="matrix(1 0 0 -1 5 5.5)" fill="#fff" />
  </svg>
);

export default SvgGroup16RightBottom;
