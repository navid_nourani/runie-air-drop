import * as React from "react";

const SvgGroup11RightTop = (props) => (
  <svg
    height={47}
    viewBox="0 0 425 47"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M6.5 41.5 47 1h377.5" stroke="#fff" />
    <circle cx={5} cy={41.5} r={5} fill="#fff" />
  </svg>
);

export default SvgGroup11RightTop;
