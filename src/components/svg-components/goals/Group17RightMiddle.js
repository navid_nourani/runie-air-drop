import * as React from "react";

const SvgGroup17RightMiddle = (props) => (
  <svg
    height={10}
    viewBox="0 0 430 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M10 5h419.5" stroke="#fff" />
    <circle cx={5} cy={5} r={5} fill="#fff" />
  </svg>
);

export default SvgGroup17RightMiddle;
