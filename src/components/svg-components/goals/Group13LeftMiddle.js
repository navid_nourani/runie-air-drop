import * as React from "react";

const SvgGroup13LeftMiddle = (props) => (
  <svg
    height={10}
    viewBox="0 0 323 10"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M313.5 5H.5" stroke="#fff" />
    <circle r={5} transform="matrix(-1 0 0 1 318 5)" fill="#fff" />
  </svg>
);

export default SvgGroup13LeftMiddle;
