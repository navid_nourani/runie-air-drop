import * as React from "react";

const SvgGroup15LeftBottom = (props) => (
  <svg
    height={47}
    viewBox="0 0 409 47"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path d="M402.5 5.5 362 46H.5" stroke="#fff" />
    <circle
      cx={404}
      cy={5.5}
      r={5}
      transform="rotate(-180 404 5.5)"
      fill="#fff"
    />
  </svg>
);

export default SvgGroup15LeftBottom;
