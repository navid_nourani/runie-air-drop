import * as React from "react";

const SvgArrowLeft = (props) => (
  <svg
    width={70}
    height={24}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M.94 13.06a1.5 1.5 0 0 1 0-2.12l9.545-9.547a1.5 1.5 0 1 1 2.122 2.122L4.12 12l8.486 8.485a1.5 1.5 0 1 1-2.122 2.122L.94 13.06ZM70 13.5H2v-3h68v3Z"
      fill="#318B8B"
    />
  </svg>
);

export default SvgArrowLeft;
