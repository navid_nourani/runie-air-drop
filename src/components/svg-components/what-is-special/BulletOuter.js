import * as React from "react";

const SvgBulletOuter = (props) => (
  <svg
    width={28}
    height={28}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <circle
      cx={14}
      cy={14}
      r={12.75}
      fill="#F3F3F3"
      stroke="#318B8B"
      strokeWidth={2.5}
    />
  </svg>
);

export default SvgBulletOuter;
