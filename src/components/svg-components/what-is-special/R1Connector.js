import * as React from "react";

const SvgR1Connector = (props) => (
  <svg
    height={153}
    viewBox="0 0 418 153"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M416 153V80c0-16.016-12.984-29-29-29H28C13.64 51 2 39.36 2 25V0"
      stroke="#318B8B"
      strokeWidth={2.5}
    />
  </svg>
);

export default SvgR1Connector;
