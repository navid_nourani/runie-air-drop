import * as React from "react";

const SvgR2Connector = (props) => (
  <svg
    height={153}
    viewBox="0 0 142 153"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M140.5 153v-43c0-16.016-12.984-29-29-29h-81c-16.016 0-29-12.984-29-29V0"
      stroke="#318B8B"
      strokeWidth={2.5}
    />
  </svg>
);

export default SvgR2Connector;
