import * as React from "react";

const SvgBulletInner = (props) => (
  <svg
    width={26}
    height={26}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <g filter="url(#Bullet_Inner_svg__a)">
      <circle cx={13} cy={9} r={9} fill="#318B8B" />
    </g>
    <defs>
      <filter
        id="Bullet_Inner_svg__a"
        x={0}
        y={0}
        width={26}
        height={26}
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity={0} result="BackgroundImageFix" />
        <feColorMatrix
          in="SourceAlpha"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
          result="hardAlpha"
        />
        <feOffset dy={4} />
        <feGaussianBlur stdDeviation={2} />
        <feComposite in2="hardAlpha" operator="out" />
        <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend in2="BackgroundImageFix" result="effect1_dropShadow_101_186" />
        <feBlend
          in="SourceGraphic"
          in2="effect1_dropShadow_101_186"
          result="shape"
        />
      </filter>
    </defs>
  </svg>
);

export default SvgBulletInner;
