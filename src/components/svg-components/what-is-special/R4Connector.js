import * as React from "react";

const SvgR4Connector = (props) => (
  <svg
    height={153}
    viewBox="0 0 418 153"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="M2 153V80c0-16.016 12.984-29 29-29h359c14.359 0 26-11.64 26-26V0"
      stroke="#318B8B"
      strokeWidth={2.5}
    />
  </svg>
);

export default SvgR4Connector;
