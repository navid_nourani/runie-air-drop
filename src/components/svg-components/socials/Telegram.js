import * as React from "react";

const SvgTelegram = (props) => (
  <svg
    width={23}
    height={19}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      d="m22.374 1.814-3.3 15.567c-.25 1.099-.9 1.372-1.822.854l-5.03-3.706-2.426 2.334c-.269.269-.493.494-1.01.494l.36-5.123 9.322-8.422c.405-.362-.088-.562-.63-.2L6.315 10.866 1.354 9.314c-1.08-.336-1.1-1.079.224-1.596L20.983.242c.898-.337 1.684.2 1.391 1.572Z"
      fill="#fff"
    />
  </svg>
);

export default SvgTelegram;
