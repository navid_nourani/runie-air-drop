import * as React from "react";

const SvgPartner3 = (props) => (
  <svg
    width={171}
    height={35}
    fill="currentColor"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    {...props}
  >
    <mask
      id="partner3_svg__b"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={171}
      height={35}
    >
      <path fill="url(#partner3_svg__a)" d="M.38.77h169.952v33.417H.38z" />
    </mask>
    <g mask="url(#partner3_svg__b)">
      <path fill="#318B8B" d="M.38.77h169.952v33.417H.38z" />
    </g>
    <defs>
      <pattern
        id="partner3_svg__a"
        patternContentUnits="objectBoundingBox"
        width={1}
        height={1}
      >
        <use
          xlinkHref="#partner3_svg__c"
          transform="matrix(.00177 0 0 .009 -.008 0)"
        />
      </pattern>
      <image
        id="partner3_svg__c"
        width={574}
        height={111}
        xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAj4AAABvCAYAAADsf8rXAAAACXBIWXMAACE3AAAhNwEzWJ96AAAZW0lEQVR4nO2dPZIiSZbHvdtaWgVWGhFGGjHpEyR9gqROAHWCpE6Q5Ak68wRFaqsVeYICbWyVBm1XahBHGlBmRdai6r0aT5KIeM/D3cMd/j8zzKanSCAi/OPv7/On4/FoWqZrjNm3/SMAAAAAcPn83OIV9o0xc2PMFOMMAAAAADFoQ/gUFp6ZMeZPY8wYTxkAAAAAsfgl8p2ekujp4AkDAAAAIDaxhM/IGPNkjOnhCQMAAACgLUILH47jucUTBgAAAEDbhIzx4TgeiB4AAAAAJEEoi0/h1rrHIwYAAABASoSw+AwhegAAAACQIiGEzwxPGgAAAAAp0mYBQwAAAACAqIQQPghmBgAAAECSaIVPl4oQPuFxAgAAACA3pFldLHimVHV5hScNAAAAgNyQCB+0mQAAAADARVAlfAorzwIxOwAAAAC4FMpifAbGmDVEDwAAAAAuiXMWn6K/1hKuLQAAAABcGqfCh91bED0gR4b0qgIFNoGEPr3aYJnQE6qbL8vEfi+Ii2Q9TW7NPRU+xQ+8aem3AB3Fs3pI/J4drIWxENTbwN83FNwTCB9wjmLsfE38zmwoBIHnVOj5ZIRrDITP9SIZH8mtuXaMTz/RHlsD4UkepEdhObwzxvxOnfrnLZ6iAcid4lA6NsZ8tuYT1kUAlNjCJxVVNqEJXZxmjsaYP+gk9pX+e0/Wgyk20ewY04INqwsAzRnTujinMAUAgAAWPl2aRG3Rpc1wT6eZ4rf0Sn7LqRVhSWIJ5MMDPTcs1gA0Z0wusAHuJQD1sPAZtXivRmTdeXAMqr4lsbSFAMqKW4gfALzRo/nU5loOQBaw8GnTT3zjKYusZwkg+L3z4AbiBwBvdMjtBcsPABWw8LmkWJke+b0XiAHKghs0vQXAGx0cJgCohoXPJVZoviO/NwJp02cMEz0A3ujQwQ8AcAZpd/Zc6VDs0IRe11hv4hMJwFAMrNPlkP7bxXX5RM9nH/C3AuCD304+Y99wjnXPuKf69P+NKhI9qrilv4UAAuCEXyK5g3a0qa1PFoiuVacnpNWJ3V+vlAYfo/BXKqwDC75znz0hS5tmwe7R38HtBVLH93zaV3zmlNbIJ4c18gnCB4D3/BxQ+BRi59EY81f6jol1qrer+c5I+Pwnvf8Q8DnB/RUHLlT4ovy2aa4XDEBA1rRGflR+RQ+JHgC8p6w7exMKq8oH2vhmCuvKnt7fDyyA2P2F7K/wTJTip4dYHwBKmTuIH5T4AOCEEMJnRCKG20xoswtYAA0cLAYa2P21RPZXUCbUY0gKxCgA5RTi51lxfzCfADjBl/AZnbSZ+Gq9/unYZoILEhaBhKuAD+7WaqOAFNAwaFxYWKgBqGamsIj3sK4B8JamwoddWV+UbSYWig1uafm3dwGf3wOqPwdjqbD63GR4fQDEZK8MWkZBQwAsXIVP32oz4ZJqeWe5maQCiCuSho7/+WwFEwJ/YKEGwB+a+QRXPgAWrsKn5yh4Trm1ugtLJmes+J8bVH/2jiYFGKZ5AKrR1A3CGgaAxc+JFIwbK9PMY8X/3CH+xxsoTAiAP66pFhkAXvklcFVfDZxmPqJgWImFgF1lXCPIR7PTczxYRfnmgb7j0kllnIFwDK1K3v0SS8OWXmtU6gYNGFrjrV8RG3iwCucuMxtzfdoPB1Yl73N73M6aV1vrOkEJKbasYDfTCwkgySCdW1ljD4F+F3d/5/YXOHGFI8WFqc4auY0kissEhc060j3klgojRVD6afXhDd23OURQMC5lrRpZL+kht0Njrnjd0//3SvtFiofYvrXHSMNJOPSE5xbvgSu6zsUV7FeDEyFssz0Rv8Ycj8filSr74/E4pd8offWPx+Mi8PUUv2ug/F2+XzPhbx22/Dv5NVDcX9fvkNwT18+uY5nQcw/9zCfH43GteJ5S5sfjsdvS+Bwqfmcbv89lTPoYD3XMAl9jl75j6320fV/HZy2OOfvVp/EfimWgdUFCyPumWYt+7NshChj6pEMp8Josqy2dCn4LmP7eIeWI7CM50hgpTbFDEJcRza/PgcoOjOnz0brELzm6mbtWuRTX7OE6OomUMZlRLOk44HfcWpnUl7BvDZVr0YH+5ttcaCJ8DlRb50PgDCvjmGXFFZk/BUp/h/jRIb1P8E2nR5eey5dAG5ANH3YWSCioRDqfNhm6EEeW4AkVt2nDZUxiV/Ef0EYcKjzjHIUA+iPzfpVPpAeka9Eb0WMaCp+pFVszoWakrw0+T4LdZFS6KD45NsyU0EGwsxhNvSaQDgPahLSdwZtyRxsRxM95pPMpp+7sXZr/XyIJnlNuaX+JYf0Z0vhuq2DrQ4aHCz6A3Qvey2wsgfmDJsLndEKxi+m/A3dYZ/OkZoDu6b2/Bkh/v0G151r6tJHVsUH2V1JM6HTYxiZkaG5B/JxHuubkcpDgTS2ku0cCW39CWkQmZLFoa14xdxlZ2Hl8aA5gG8sl9oYmwqfMfPp/9EAP9L9D0bPMk9LTz9pqf+FTnOVsNoyB9P485X2ZF8WE5lfbQPy8R5pF95JJNk+f1uaU2tU8BFqPBonMK+YmA3HcdbCOseg5q1NCprOzmv2HMeYvAb/n1kp/nwknOrvoZkqzWRm9c+Y08I2J8BS3gptLRIyNbNBg0X+1Ukd50enSZ3LKqfaky4vzyPE3XRJdxTzJ4UDWpbXYNXbsYKVr29YLLvswbOCmvadx7Gtd6je0sKysOkT2XtP0WsdW2ntquIie2lI4Mer4sOgJLYDG9HqkRbsuoG9PN4cfeFOz4wjC5x0j4QZ6QCaPmNDCp+swH3b0nKtq8diLKhcp1SzSd/Q312wV7CuezWMm1p6Fo6VnRcJOIiS6dACbOgisz5aQb4rLPrOy6lxJ6NJ1TpXfxW2jUguEnzuInlo3cMx09kL0/Ctw/I+x0hOlGym7ypr+LmR3/Zu+MkhxCtGYDDPl5vBoWYiki+aC5twHZcmJa20dw6ndUnfQJhNrz8zBQrGhUiVDhfVkbyW5uGT5+ggCnik38J11nRqLk93PUlMapJPg4XMujA1lRKLHRBY+Bf8RKf6HU2K3wviftQfxc80xCFw+ni1ompoUz3BxJcNQ4fo9ULLArMEpcaFcoDtXYvHpWvNprkzt3ihiHtuk75DG/ULjpYm76MlBFPQaCsm+UlS8erjOrUMz72lC+9hcGej+SZNk1FbLiljxPz2K/3kWDDzOEvsS8Pe0wVfFd9ZlvHU9ByC+wMWVFNLFvTJwUMmeFmjpQjdWxPLFZlCzcZz79+HJvzdxuft8LqHRHnbEp3kBW4d08nsSTS7jbqZ4rj6v09BnDYTX2SE3dNsHUa3o+aj9za7Cx1d1XRY9/2uM+ZunzzzHvdUDpWpRWNDmH7tmSSrEvO5PGZ3er8GNORA+/12gzXWqWKBnLZeQOLb43WXkJHq0Qbi+xYCh+6QVPy7jrq/YxFeBxjWndEvEV9vCJ7joMQ1cXb4n13+RPzNkuwJpzQKkpoflQM86J5fFNbgxpZa3UaDNdU+fLXE3j5He/oZnEo25VGjWbO6bgCJ3rwxxGDtUdpbuJ4eAWYt7xT3UxNT4hJMqpKKH9xEnkZZSry5u//AxYI8tSc2CJfpFBWPnIaUThEGy6D4GDkLfKgQxioZ+57fM3MVd5Yk+9HPWiAKjvNddhZhpEisnYaEo3hs7RoxT1qWi66AMbn9Hik1KOa0ulAAaCwYjNuYw9CKWhAdyRgIz+C6SNfRJeALHGPrOwspYygHNc3uJlO2pEQWa3y+ZV8YqBxEa6fyNKXy0dXre9d1yIeXu7HOyAD0GSIGvG2QQPuHgitttd0QG/0ay0MXy+++Fm8AN3F3f6FAM45/0jFK/Jxp3TsywA+l3dRTXIH1fLLf/UijwYsU0akXP2b5bLrSV1XVKmdDgmgRP9PLVx6VHm27ZYp5D4a/cYQE0oRfueXtIFuiYAY9zYarzMLMmnKFha/Ys4Rg6aVDzKvKasFQktoyE407quok5twpXXd18j1UdXlOc0GvwfirCp469JVTmDcqb20wrBhyK6cWDOyKPYGlrha5wPsXchLa00NUtihA+7+EaZkNBFmtsNC6UNjKL5kLhI7kO6bWuIj8jX1Wom8A1iqQp/t4zFlN2dZ1j6VCUqYybjPzil06H6g3B9RUfyQItjX/wiUQEo1p6OXcJNnfVPK82BO1cGFbRE+wdUuFzbYc9reh5CVGmIReLj83eco1oK3+eoi0HniO7CKf1vicrnM++OECGZDNq44AgGQNt1dvSCsHTppI2/ZL766NY6I11WEwB6e94bdFSJc0uGtSsq9JrvSbhoxU9hsaB97GQo/BhOP3v9wafcQ0Wn3kLtYmGNMgnDov3MtFmeZeKVvjEGkvSuekl2FFJ7HRf7mw/cqizckPxPimkvEufaZtiYKEQPlVWKanwuZZD3oTGobYy+T3dZ69jImfhY+hGDhsUXcqhp02OLOnFz+dJIYC4HxPcXnHQukKaWll907+CzYOtoFzqY6ZM9GjSbsEn0rHWZtyWdCzViTiJBfxwJQe8CVnzXZn7LtCZW4zPOSYROr4Dd7j7vSYua4z4jWjk3p7l2sYJl4H4qPy7FCrSSw4/MVzzVfgQPrD2vKWJ6DEemsS+I4TwiT1opXU/QHtwXNar4hegeel5EJD/lmu9H3Ol+AnVDsE3KWTpNa3cj/pS/rn36aG5BOFjriBA+VLQWOfQj+k8ED5vueb7MVccJjqZuPZTED4Sl0qVpVS6biGJQ4e3Ap2puLqaLl5b9NfKgr3SkoMYrLBcgmjI3VXXFM18gvs4DtL7jAQOHd5cXqkIHx8TEsXv8mCu6MEG4ROWS7GWXLNlcKuw+uRwn1KIRQJxWFGT3Q+Kb/Pi8kpF+PjY4GA2zAepORsnVCDh2seJdD7hIAFSoAh3+GR1WF8o4z8bu7xSET4+qiij11M+SK1ziGcJy6WY2q9d+ORy6Luk7Fup1Rq85WCVOLGZKO5pY5eXq/AJYTJtWrcF/tJ8kIpUH9WgQTnSDdMWqj8l+Lr2rM5chI/kd6Zw2JHEjeGgrYdFz7lxsFdqgEYuL1fh07SU+jmmDQc9XF35gGflzrVbN8Bl0/Zhx4fw8lUE8dIY1dyb4oD1rLhmZ5dXSgUMOzi5AVALUvxBruTg4vbRXFTqfbgm4fMifP4zRYa2s8srtcrNd5Gj+mF5ABJSMmv7tvi00XkdXCdSQdCmVVMqfKrWhByuMzbSfT2KyytEr66mYuKBBlWMooSIC2qH3LJLUhE+fYcmf3VsBTENcK8BH0j3hmGLhQylFa6rrkV6nR2a07HXlzrLy9pz5fyN8hqL739U9AVU9/IKYfHxISY+oxrzRSM18cIa8ZYQglGySHeQYZc0uQhTqaurrfYaI+HB4iCYN1J3TexDYHGNtzUv3+PJRcQGdXk1ET5lD8xXIcExDS7Unrg8pM80J4tcDGEQYkOQztc25uGAvrfqBeT3IQXXvuQw02tpvEnnl2TOpCryJJYc3+PEVRNoWhypXF6hYnw0nbirKLLHvpL1p25jQdBnPvgwJ6dG6GyULsXA+WYtXFxiL9DFfP+D5n/ZC8kQ35E+mxQOEtLTf+wKzn06bEuQXIN0s7+LuHf1W0rVd/28tXIciLO8Qlh8TAA31di6CWUXhjiEPJgo4lRya0MScgFrWueqCslCfhfZ3SVZ8JCcIN/MTCLzSSp8biNbfTQbrE/hYzzH01QhvUbf86qJkHpShDyIXV5NhE/VKWOpLEEtoWMFPofcBEBYpJPvkKHwCblQh1wcpZaTWKfwofD0jf58ukNmCvdL01A6lkVPY+15EVrO9oo9cBrB6jNQXGNq80qz9olcXk2ET12bCU0Jag0dCn5OTQDh9FnPk8Il1FZWRxNCCZ9JYFfaWrgZjSOdwqUbXo5jxCdThbUnpUQB6fO9iSS2NeIxxHs7EZJ5pPfct8HCB5zlJaXW5dU0xqdKie3JKhSqP0uPBNAykQBopMZXMyE1LiW1rD7JOA4hxLuRFn/pwti4QaDg8yWV4V+vfM4VY+13xftTEolzxb7wEPiAO1OKR401ZKE4/N8FvE6NQE71MDHz2curqfCZ1CyCnJUVsqHbrUGgY+rMSKRK2SVobpWmfft2SWmsZE2YC+dpj55NqH59PgNMLxXtfDIJHiQ0Yv5zIFEwUdSKMY4HEO11+j7EDxQCeZd4GRlvhQ2bCh+JiW5NNz+0CQ1ZXWnRpYG6VS4uJmKwnwapdWHmMdB+rhACPpAuLDckfnwGO08Um3nqC3QouMGjdj5J41Ji8qQ8EH/2vC48KcXjq+NhbK6IaTJ0iPcl8gbK35z6nFoqXbal1mmu3LxrcKpkE13VTWO31yjiCRZ8Z1hz6mhiWeme2eRZZUtNq6esEj3NL4Xp5B3L/eoa9zVQuHx8wkkJkuu8sTItm1hb2ZWncYPGTnVuC55fvHa6rJuHhO/XhDZ6Kb/TfZg0yBQa0njVzK1DQ9E1odIMUj7Tc581EKwjWkOkGbS7TOZVcS//FL6XXV7vnh0Ln21DMcLKuU4xLmhxndLLd/l9X7iYG1Pq52RzWyNCtKfHkBwSzthbKEzGHVroHmmR1fTumUa28pwyIUEjWQ86dE+mdJ1z5UI9oYVJs/ZsEjiZhnTDuh4YypglvDZxN26N6L2lje+Fxpz0cMGCyaUWVhOhZRxaMBi6JxO6Rs0a0qf3a68zl0zpLT176Rp5b+mOH/x0PB4NLVyaQLkyXukGSh5Snyalr0XexWr1WKJyh8qTSMGvkTO7ZomJFh98bLipSe7JTw0+f+m4Mb3S2ODJx24ifg0anOjP8VvDzZlN5C4Hkw397ZoWqa21aQzJijF0vN5DQ0taFS5zPnVWHmJGjjX/XraGalg3sG5yPOCWPof3HraWcfVv10P2s0cXm+v6YazA6m2JCOM55XIfm1xj3fgwAfbGPn2expr1ppcXW3x8nWDurDTzOnfF1nKRaU2PqbFDOntjmoqeGMwcN8c7evkQqtqTowvcpFAbQGtoHoeay1PMMzGbFnteaRnSHuQybnoBLaQvnuOKRg2us85y78oqQkyl7/jbLWkG6Tr4zuXFwc1rj3UeChX2hYSP5IKXpMY+Bkx9L8NnECpwJwfRYxyC63wT8z7N6ftSIZcxkgIbEhO5pPvv6fdqgoBD8xLA/ZPadeYkjk/RNDE1p1ledlaX73Rwtv5I1ST349IUKmqKDyV6QCq9M8W9+5DZhjYKXJ6hDN74Y8ZrzOn5xD6QnALRI+c1M9HDpCQKHgPGvPB1tl1QcpXpOLHR1gn8keVlC59FgIfBwY9LoXVlT0rury1XkNRYgppE3l8zryR0c6vHso8sfg4Us9PWxr9ocUPKURi3RXGvPtHYzHU92tPaG/Pwa8NzLXR2E4uftq7z+QJEj6FD4FAhfn4UNjyt46NpA6/hlrJcqpqMnl7QiAZhyAW3rA6JtD7JK6w9al7puea8QHNtqtCntlVJLY7YImTdwkL9kqkwjs2Bnkv/gtaiGQXExrSKPNM9jFk4NfZ17mjtjV0nLeQ91RZJ/ubyOhU+28A+vwf6odLvsON/Qpywy7JKJBafDZqlijiQ2PlElrxR5MUlFHxqCzE2D/S5wxLXVhuC0bbGhlyoV7Q4S7NDr5EdCcMPVh2kS7tXa2t+hRxvLzSmpy3dQ/s6Qx1odvT5IYRd225wYx1En4Xvn3M6+ymaKqqurJT1Ebo19X9cizCeS2+uS9FLIXhw6Km8+bkihGXUTZr9SdbNOvI9ktyTUGZsrhPSJHV2Q6f2Rc19m9RYJWPEAnHNoZGHelwHuuY2a870PRxkBsq4wbLU5Kr3xZ5TdfNlGekgM6Dn46PsA1f+jh0zJ8HnvHqleRXSVSwZ8zEPun2r4Ofg5B4eaP6UCh/TsJaHBm2Rt7L6P76ET109D591HcBlwgKMF4WyNNQNLbxLWqBSLTRXh73QSFN1V1ZtI7izgIaBNb/6gjTvlVXnJ6d5prlO3tTXlhiFxbSEKuFjaNGeO1a71LAjMaFZALkVAw8GF+GzO3NyLiuCdxDWJwLg2mGrW9+aX3zqO7UKAuAL23ottaaBK6RO+DCnIiMUWveXscreGwfh83om3uhcdc2XFn3AAAAAAPCEtDs7N138EDiNl/uwPCl85XNS+X93+L5z1htb9GwQaAkAAABcDlLhwyzIdB0qy4q5t1paSChEyf8ov+NwRvgMrX/75NDWHwAAAAAJoxU+jF1lOVQ6W4cyy9aespdOORdQPbTqh6A+DwAAAHBhSGN8qqhLM/fFK31HWfyPplv5u26tRBcuLQAAAOBycbX42HBhsz5ZS0JxR9YfafXnKspidiB6AAAAgAvGh/Bh9iQoQvbZ6jhUfz7lEXE7AAAAwHXiU/gwMfpsFWnrXxTNT5mXCA3oAAAAAJAoPmJ86phQoHDI+B/u0XFf8Z4X9NYCAAAArpsYwsdYjfSqhElI0GYCAAAAANGEDzMk64+0n09TDiR4QjZpAwAAAEAmhIjxqYJjch4jfNeKvguiBwAAAADfiC18mMLt9Wug4GeuujxEkzoAAAAA2LQlfIxVkflZ8F4JB7IkoeoyAAAAAM4SO8anjBG5pFwyvzYkdBYoQAgAAACAKlIRPoYyvyb0qgp+PpC1aEEvuLMAAAAAICIl4XPKucakWwgdAAAAADhhjPl/Zsp72sWTuBgAAAAASUVORK5CYII="
      />
    </defs>
  </svg>
);

export default SvgPartner3;
