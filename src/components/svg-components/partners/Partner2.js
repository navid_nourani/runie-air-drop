import * as React from "react";

const SvgPartner2 = (props) => (
  <svg
    width={52}
    height={48}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    {...props}
  >
    <mask
      id="partner2_svg__b"
      style={{
        maskType: "alpha",
      }}
      maskUnits="userSpaceOnUse"
      x={0}
      y={0}
      width={52}
      height={48}
    >
      <path fill="url(#partner2_svg__a)" d="M.404.132h50.604v47.739H.404z" />
    </mask>
    <g mask="url(#partner2_svg__b)">
      <path fill="#318B8B" d="M.404.132h50.604v47.739H.404z" />
    </g>
    <defs>
      <pattern
        id="partner2_svg__a"
        patternContentUnits="objectBoundingBox"
        width={1}
        height={1}
      >
        <use
          xlinkHref="#partner2_svg__c"
          transform="matrix(.00579 0 0 .00613 -.004 0)"
        />
      </pattern>
      <image
        id="partner2_svg__c"
        width={174}
        height={163}
        xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK4AAACjCAYAAAATkiUwAAAACXBIWXMAACE3AAAhNwEzWJ96AAAM80lEQVR4nO2d7ZHbRhKG+1z3n3QES0ewvAiWikBUBKIiEBWBsBGYG4GpCG43giMjOG4ER2awjEAu2D0+GAKGGGCm++0Bnqot2yqXyAUajXf68x/fv38nAyyIaEVEy8rPzPO1X4noTEQH/jlZ+CUzYc73yN0v99++++W48H0jvm/un+fKn/8BsuGWv+yGiNZEdDfw7yovyDMR7ScjTsKC71N5v+4Tfs7ROSQ0w53zL7+NYKxtlL98UXmiJ/qz4mv5IHwNP6AY7pyNddvxlRKDIz8k5/QflR2lwe4Se9c2XkrvjmK4C36FSxmt48oPy174c60y52v1Xun7X1hCvv0EcgHP/MqRpnxQfpsMtxMrvk9aRkv8hnwr/wXFcIlfPUelz/7Imneu9PnolAbzH4U3YpXH6rkE7XCmJRkcR/YsE/9nzw+2Jq8sEf4CyeOSomRwPEyy4W/sAIz2yh7/b6AZLilLBuIbtVX8fBRKY/kM8F2Kptg7agJCWzJcWTKMNVmxYk2rTat0Q86clV7vV8XP/0FXjYQ5SzbNgxix81i4KEIdRKng2HGwWYt7Zb2txR7AaKka+moCvcgG4en/14gkQ1lv8G+A7/Gt6UBWxUJ1mPbF/Ctbo/gdJJjzA5qqRqQrna43slRwPCtLhruRhMhSFjaFsO7iJCwYLvFr46r4+e8zD5HNQX6/x66yzIrhvt3SPAIUGUcZJKvy2ngNOQxb6YBwPCsXebxyXDE3vfumbLhXdgqdS0yteFyHtmTIMUS2AfC2RWhdtDWPSyAhmw/s/XPgrHwoe+F7GoQ1j0sAUQbiKEMOJZCFstE2FtB0waLHJZDERC9PAUSpKf+r/HV6v7kselwCiTK8N2y4cwCp8zTkO1j1uA7tKIPVrNpBoTO3yuDojFWP69COMtwZTExotJPX8RbQdMG6xyWQ2tFfjLS5I+jaxxghResel/i196T8HSzEducANRfHWNcqB49LAJVN3qJnEHbKrTjB2TEfOXhcAogyzMAjDCuA/rFtTDmVi+ESgGRANVyE0NdLbJmSi1RwaEqGK2g2TTtkmERG5eRxSVkyIPRp1dkqGy11LQwPJTfDJZAoAwILgGjHU6pxrrlJBYeGZECTCielMaCOpLXLOXpcUpIMSJ3AhbLRUozsmI9cDZcUJANKfW4ZK/2q/B069471JVep4JCUDD8DJCAQWsxFJl7m7HFJUDI8gWTNdlYLw0PJ3XBJQDJcQWoV1gAjQaNmx3zkLhUcczbgFAeWTwDFK6PrCBmL4VKikj6U9h3twnDxIqMxSAXHiU+7sbgAtA8Rv561C8OTZMd8hHrcBf808cPaSlBiBOZRBj8v2dtqSoQnjS4Qn+Gu+GfBF6jLzb7yhdwDzx2IIRkQdC3lnh3zUTXcdcVYY1yMCz+JiAZcDAjSR2k9iYB2YThpzg6uGm6qU9pL6vRfT/p4q5sDh4VA6LP7wg+PChKGS/xK2YDl80MlA8oOtNFkx3xUowrfEn7OPWtfpDGdIVGGV6AOh1JbjyI75qPqcSVWNCGuYbolGZBGi04D/5iqxz0LaJYZ4MA4n/e4AunzBUAk4wXlsN0UDpMIsaDtzG2KMqC9HbSzY1DjppoyZxL65UHzRNpAwZLAgWa0WYxNikmT4Z441JGaz2At3Zvav6MYLUJheLLesb74MmcS696jTjeJwJa9Csp6qJRVbV2BXA17q1ZBQleh6V0kRp0d83GrOmwtMMbzYVqz3wjC2KQvqOtgu1SHSY2mHNPO3FsgFIZDvwm71OPGrmNtYwxrR7uivcEcIjvmo2shecFPYErGuma/DsLYJLHesb6EFJJLpIRp5JJB6hr7MLFNKLQDQiJXDhl+EUK7MNzCgOo/CO05k1iOdw+WVZMi+7FJMenT5StVD/oOLVuTEISlIihF8p3o254uUYFvdYdYKAiF4eaudd/29INAiOxuJFEG7bFJZEkiOIYOBJE4TOS0qbwOQmG4Snv5UIYarlTXhImTbiAI2TGzcmzoJJuzQMxvlmlW7Rlgb4Q5ieCIMYLpwAMyUmJ5U3kTCGOT4GpsQ4g59C51/W4ukgFhbJL5iE3MoXebxC3us0wOadoFNKQxpC42sac1pjZe67W7CNkx2BrbEFLNx00pG9DafbqCMDbJVHbMR6r5uJuEBzaLUQaElfvZGC0lHuy85xLF1w7/byjWJIP22KQvORktCUwkP/Fr/ROfZIdyZc/xzlAF2VqxMPzIziO7ajvpHRBrfvJDbuSFowkHo1GF5w4jp95uHJiWlb+jDAn6vPeFr9Uu54J8zeUlK89ofncjTyOoDutL07UbSxnoqLbuTGTEmLbuTGTEZLgTJvmngS/thlK4A8qKD3jWEhBtuN/Lt4rLaX0rK7mSo2m4c75pi9oP8Z/78vlroyGeVW0NV5/YbhkSPFWiLKNs5Zc8nM1rK6mGBOQvHu+ESMrhdS5cuB+TEUsY7pxv3DpyVdQvhl6bpbT5TeBzjlzIk31YLLXhbthoY5fxmZi2UuNNsJwxewNOGVVwXib2zYIfyNaCpCZ/4Eq0HdiimGik8rgpX40oe3RDKTX5/xQ+98Jvp6z0bwqPm9Joj4YbJ89C41rr3PGUnKyGZ8f2uKkPITlMctQcbDcVkjewTGy0T5m87iTWE7TxsWO1GjyxPG7qwSC5DQWRCo+1gbTmtRcxPO5cYLhFkVl54z5xU+kt7q13TMfwuKnnKeQ66BlhSqNZzTvU424FlvjlukrqDSCJ8tHqRMwhhrsU+KW/ZZ6+lNpo5OOrRa87RCqkDutYnZ/QB+3N6GhLt2/S1+PuBGKRuxHVnmqGyMjirIo+HldqjL6lssUYTEOeAwj1uFITWbIaXtGRZzYcTT5bWQgeariFQPjmZUxt1jW2iSb/hLC3kFkLMVyJbd5WSxZjslHWu3cW5EKIxj0LeNucF5WEoJ0SJvSCppAl1BISYTLaP9kLbPC8BXQzahePK7H1MNfNOkNASAnDvgG7eFypKAKK0aK0uyCkhGG97i3D3QokGp6AnuoNH0BRvs+JZ9tqAXtQ80kFieV7SJVf9d/3C5DH0UwJQ8o4n8dNvR3mAhbsrv++BVD2TjMlPEP0um2Gu078hF/BVhY1LcxDyt9r692vaCn4JsNNndZFq0RaeMozkXZNHJRTwlAHtSaN+5x4ZwHaXIRb+hHtQdPsEn6Hko6vG27KCiUnD5DqEEpv+muH/w/5ECkJTNVeVSqklAjuIIZktCEdHPdALS5l6l1LvtyhXIeqx00hEa6sjXaAWbE+r1yk/H3qJtU2IDpTnOHGlAjH2uBhRAo+KYeCJBk0U8JH7VBmabhzfnqaNNOl4cmqr3A6tPw5KkNrLx6BZINEHUkbqnUMpeGuavoz991iMU7lMKfrgANmbFQzamPbc9ZXItS5sLdDecC1UsJqA0XGZLixX6tIjYU+uZcalbfPmAw3ReAeSTJIdF83ofL2GcuCvlRzIJAaC7VSwiqx3TF43NSeCG1wnFZKWPTtk7vhSsU6kVpctFLCopIhd6kg0eRJYJJBKyV8J1lBlrPHlT6soO1e00oJi7x9cjVcrXQokmTQugYiiYlcpYKURKiDJBm0uiZEOkdyNFyJUVFtzAAjDBpdwu9TX4fcpIJmBomA91VopISTlj/m5nFTdyb7uAIvxtboEk4qGXIy3HXiXrlbbIAnqGvp3YdUWbVcpIK2RLAyyXunpP+jd47kYripO5N9qHcDBKKREo6eVctBKmhKhAuwrm1DY3B09EIc6x5XWyJY3eauNTg6WoLGusdNvUPYxyfD29y1BkdHS9BYNtymeV9SfLO2F6yBDUsdSWaxPK5VqaA5zSWnpdhaXRODR7ha9bhaiYarsQjCLQ5Ku4SLoQ+/RcPVlAhIo1FjUXBIT5LBWTVrhusbCZqax4wXB2qEyAbNY7OmcbXmB1hLMvRBa5dwr141Sx5XSyKMZdvlM0dLpOkVIrPicUc7I0sYra6J4E5pK4ar1XKN1kcmgZaTCHIQFqRCoWS0Y12IfVIKkQVJBnSPqykRkPacaaBxEO78hkM3XC2JkFN2rC9aBUydJAOyVNCSCGSkKDw1Wl0TnSQDquEuI82x7cMx40RDKAeFLuFOWTVEqaC97h5pdCgKGlNxvPcB0eNqDfOgydu2slGoZ/B6XTTD1RzmQSMNf3VlzYdWKbztPkiGm3qH8C0egdvLEXhjxyJpvNu2TZZIhqspEV6BVkAh44xXqu1n1hZLRzmcaVXiE+AGHSvE2mDUhR8OagiGqxlFQNuMbo0Vy7vU9+6H5dcIUmGnZLQuOzYZbX8ObFCPCQrRX3lC0IcmnavtcbWKl5HWmubEpueAlmtl/7P7p1e6aRqudC78yjnwYooeiLDkn8aoQGX1bq8VvJqGu/XkwrskAZaVnPay4QFwC7TdE3zzKZ4wAhH9DlDjELSaOt7JAAAAAElFTkSuQmCC"
      />
    </defs>
  </svg>
);

export default SvgPartner2;
