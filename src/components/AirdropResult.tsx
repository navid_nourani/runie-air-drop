import { Stack } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import WalletAddress from "./airdrop-result/WalletAddress";
import { Divider } from "./shared/Divider";
import FormContainer from "./shared/FormContainer";
import Text from "./shared/Text";

const AirdropResult = () => {
  return (
    <FormContainer title="Airdrop Result" marginBottom="29px">
      <WalletAddress marginBottom="19px" />
      <Box display="grid" gridTemplateColumns="5fr 2px 4fr" gap="10px">
        <Stack alignItems="flex-start">
          <Text>Total entry point:</Text>
          <Stack width="100%" direction="row">
            <Text>Estimate reward token:</Text>
            <Box flex={1}>
              <Text> -- </Text>
            </Box>
            <Text>RUNIE</Text>
          </Stack>
        </Stack>
        <Divider />
        <Stack alignItems="flex-start">
          <Text>Distribution date: June 10th 2022</Text>
          <Text>Whitelist:</Text>
        </Stack>
      </Box>
    </FormContainer>
  );
};

export default AirdropResult;
