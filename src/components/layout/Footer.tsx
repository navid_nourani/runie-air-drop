import { Container, Grid, Stack, Typography } from "@mui/material";
import { Box, styled } from "@mui/system";
import React from "react";
import SocialRoundButton from "../footer/SocialRoundButton";
import UserSocialCard from "../footer/UserSocialCard";
import SvgLogo from "../svg-components/Logo";
import SvgFacebook from "../svg-components/socials/Facebook";
import SvgTelegram from "../svg-components/socials/Telegram";
import SvgTwitter from "../svg-components/socials/Twitter";
import SvgUnknown from "../svg-components/socials/Unknown";
import SvgWebsite from "../svg-components/socials/Website";

const telegramIds = [
  "RUNIEGlobal",
  "RUNIEThailand",
  "RUNIEIndonesia",
  "RUNIEIndia",
  "RUNIESpain",
  "RUNIEBrazil",
  "RUNIERussia",
  "RUNIEPhilippines",
  "RUNIEArabic",
  "RUNIEBangladeshi",
];

const Footer = () => {
  return (
    <Wrapper bgcolor="primary">
      <Container>
        <Stack>
          <Stack alignItems="center" direction="row" marginBottom="75px">
            <Box flex={1} sx={{ "&>svg": { height: "100%", width: "45px" } }}>
              <SvgLogo />
            </Box>
            <SocialRoundButton
              marginLeft="26px"
              icon={<SvgTwitter />}
              url="https://twitter.com/RUNIEOfficial"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<SvgWebsite />}
              url="https://runie.life"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<SvgTelegram />}
              url="https://t.me/RUNIEChannel"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<SvgFacebook />}
              url="https://facebook.com/RUNIEOfficial"
            />
            <SocialRoundButton
              marginLeft="26px"
              icon={<SvgUnknown />}
              url="https://runie.medium.com/"
            />
          </Stack>
        </Stack>
        <Grid gap="23px" container marginBottom="66px">
          {telegramIds.map((telegramId, index) => (
            <Grid item key={`telegram-id-${telegramId}`}>
              <UserSocialCard
                telegramId={telegramId}
                url={`https://t.me/${telegramId}`}
              />
            </Grid>
          ))}
        </Grid>
        <CopyRight
          color="primary.contrastText"
          alignSelf="flex-start"
          textAlign={"start"}
        >
          © 2022 RUNIE. All rights reserved
        </CopyRight>
      </Container>
    </Wrapper>
  );
};

const Wrapper = styled(Box)(({ theme }) => ({
  paddingTop: "62px",
  paddingBottom: "34px",
  backgroundColor: theme.palette.primary.main,
}));

const CopyRight = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  /* identical to box height */

  font-feature-settings: "liga" off;
`;

export default Footer;
