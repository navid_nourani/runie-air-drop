import { Stack, styled, Typography } from "@mui/material";
import { Box, BoxProps } from "@mui/system";
import React from "react";
import SvgTelegram from "../svg-components/socials/Telegram";
import SocialRoundButton from "./SocialRoundButton";

type Props = {
  telegramId: string;
  url: string;
} & BoxProps;

const UserSocialCard = ({ url, telegramId, ...props }: Props) => {
  return (
    <A href={url} target={"__blank"}>
      <Wrapper {...props}>
        <Stack direction="row" alignItems={"center"}>
          <SocialRoundButton
            customColor="#C49A6C"
            icon={<SvgTelegram />}
            url={url}
          />
          <ID color="primary.contrastText">@{telegramId}</ID>
        </Stack>
      </Wrapper>
    </A>
  );
};

const A = styled("a")`
  all: unset;
  cursor: pointer;
`;

const Wrapper = styled(Box)`
  width: 291px;
  height: 50px;

  background: rgba(49, 139, 139, 0.33);
  border-radius: 25px;
`;

const ID = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 16px;
  line-height: 24px;
  /* identical to box height */

  font-feature-settings: "liga" off;

  margin-left: 16px;
`;

export default UserSocialCard;
