import { styled } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";

interface Props {
  icon: JSX.Element;
  customColor?: string;
  marginLeft?: string;
  url: string;
}

const SocialRoundButton = ({ icon, customColor, marginLeft, url }: Props) => {
  return (
    <Box
      marginLeft={marginLeft}
      bgcolor={customColor ?? "#318B8B"}
      display="flex"
      alignItems="center"
      justifyContent="center"
      width="50px"
      height="50px"
      borderRadius="50%"
    >
      <A href={url ?? "3S"} style={{ fontSize: "0" }} target="__blank">
        {icon}{" "}
      </A>
    </Box>
  );
};

const A = styled("a")`
  all: unset;
  cursor: pointer;
`;

export default SocialRoundButton;
