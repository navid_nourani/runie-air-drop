import { Stack, styled, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import PurchaseBUSD from "./private-sale/PurchaseBUSD";
import { Divider } from "./shared/Divider";
import FormContainer from "./shared/FormContainer";
import Text from "./shared/Text";

const PrivateSale = () => {
  return (
    <FormContainer
      alignItems={"center"}
      display="flex"
      flexDirection={"column"}
      title="Private Sale"
      marginBottom="5px"
      paddingBottom="16px !important"
    >
      <PurchaseBUSD marginBottom="8px" />
      <Stack direction="row" alignItems="center" sx={{ marginBottom: "3px" }}>
        <Text>Estimate:</Text>
        <Box width="70px">
          <Text> -- </Text>
        </Box>
        <Text>RUNIE</Text>
        <Divider sx={{ marginInline: "10px", height: "21px" }} />
        <Text>*Min buy: 50$</Text>
      </Stack>
      <Box marginBottom="3px">
        <Text>Close time: </Text>
      </Box>
      <BottomBoxWrapper>
        <Text>First Come First Serve</Text>
        <Text>
          The private sale will close before the due date if the allocation is
          sold out!
        </Text>
        <Text>Duration: May 1st - May 3rd 2022</Text>
        <Text>Price: $0.05 / RUNIE</Text>
        <Text>Allocation: 16,000,000 RUNIE</Text>
        <Text>Distribution date: June 2nd 2022</Text>
      </BottomBoxWrapper>
      <Typography>
        <A>View RUNIE contract</A>
      </Typography>
    </FormContainer>
  );
};

const A = styled("a")`
  font-family: "Secular One";
  font-style: normal;
  text-decoration: underline;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  cursor: pointer;
`;

const BottomBoxWrapper = styled(Stack)`
  border: 1px solid #fffbfb;
  box-sizing: border-box;
  filter: drop-shadow(0px 10px 10px rgba(0, 0, 0, 0.25));
  border-radius: 35px;
  width: 100%;
  padding: 7px;
  align-items: center;
  margin-bottom: 6px;
`;

export default PrivateSale;
