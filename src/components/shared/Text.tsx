import styled from "@emotion/styled";
import { Typography } from "@mui/material";
import { TypographyProps } from "@mui/system";
import React, { FunctionComponent } from "react";

type Props = TypographyProps & { children: React.ReactNode };

const Text: FunctionComponent<Props> = ({ children, ...props }) => {
  return <StyledTypography {...props}>{children}</StyledTypography>;
};

const StyledTypography = styled(Typography)`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
`;

export default Text;
