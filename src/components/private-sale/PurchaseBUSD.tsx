import styled from "@emotion/styled";
import { Button, Input, Stack, StackProps, Typography } from "@mui/material";
import React from "react";

const PurchaseBUSD: React.FunctionComponent<StackProps> = (props) => {
  return (
    <Wrapper {...props}>
      <AddressTitle>BUSD: </AddressTitle>
      <StyledInput />
      <Button variant="contained" color="error" sx={{ marginLeft: "8px" }}>
        Purchase
      </Button>
      <Button variant="contained" color="warning" sx={{ marginLeft: "8px" }}>
        Connect
      </Button>
    </Wrapper>
  );
};

const AddressTitle = styled(Typography)`
  font-weight: 400;
  font-size: 15px;
  line-height: 22px;
  /* identical to box height */

  text-align: center;
`;

const StyledInput = styled(Input)`
  border-radius: 35px;
`;

const Wrapper = styled(Stack)`
  flex-direction: row;
  align-items: center;
  & > * {
    margin-left: 8px;
  }
`;

export default PurchaseBUSD;
