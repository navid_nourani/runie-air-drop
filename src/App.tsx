import { Stack } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import AirdropResult from "./components/AirdropResult";
import Background from "./components/Background";
import PrivateSale from "./components/PrivateSale";

function App() {
  return (
    <Box
      position="relative"
      width="100%"
      height="100vh"
      justifyContent="center"
      alignItems="center"
      display="flex"
    >
      <Background />
      <Stack position="relative">
        <AirdropResult />
        <PrivateSale />
      </Stack>
    </Box>
  );
}

export default App;
